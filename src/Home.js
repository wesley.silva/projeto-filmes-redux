
import React from 'react'
import { View, Text, FlatList, TouchableOpacity, Image} from 'react-native';

import {useNavigation} from '@react-navigation/native';

import {useSelector} from 'react-redux';

import StarRating from 'react-native-star-rating';

export default function Home() {

  const navigation = useNavigation();

  const filmes = useSelector(states=> states.FilmeReducer.data);


  return (
    <View>
      <FlatList data={filmes} renderItem={({item,index})=>(
        <TouchableOpacity
        onPress={()=>navigation.navigate('Details', {index})}
        style={{flexDirection:"row"}}>
          <Image source={{uri:item.poster}} style={{width:120, height:120}}/>
          <View style={{left:10}}>
          <Text style={{fontSize:22}}>{item.title}</Text>
          <Text>{item.genre}</Text>
          <StarRating
          disabled={true}
        maxStars={5}
        rating={item.rating}
      />
          </View>
        </TouchableOpacity>
      )}/>
    </View>
  )
}
