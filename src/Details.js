import React from 'react'
import { View, Text, Image } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useRoute} from '@react-navigation/native';
import StarRating from 'react-native-star-rating';

export default function Details() {

  const index = useRoute().params.index;
  const filmes = useSelector(states=>states.FilmeReducer.data);

  const dispatch = useDispatch();

  const handleStars = (qtd)=>{
   /* dispatch({
      type:'SET_RATING',
      payload:{
        index:index,
        stars:qtd,
      }
    });*/

    dispatch({
      type:'SET_TITLE',
      payload:{
        index:index,
        title:'Teste',
      }
    })
  }



  console.log(filmes);

  return (
    <View>
      <Image source={{uri:filmes[index].poster}} style={{width:'100%', height:250}}/>
      <Text>{filmes[index].title}</Text>
      <StarRating
        maxStars={5}
        rating={filmes[index].rating}
        selectedStar={(qtd)=>handleStars(qtd)}
      />
    </View>
  )
}
