import {combineReducers} from 'redux';

import FilmeReducer from './FilmeReducer';
import UsuarioReducer from './UsarioReducer';


export default combineReducers({
  FilmeReducer,
  UsuarioReducer
})